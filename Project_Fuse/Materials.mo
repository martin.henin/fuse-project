within Project_Fuse;

package Materials
extends Modelica.Icons.MaterialPropertiesPackage;
  
  record MaterialProp "base record of material properties"
    extends Modelica.Icons.Record;
    parameter Modelica.SIunits.Temperature Tf "fusion temperature";
    parameter Modelica.SIunits.Density rho "density";
    parameter Modelica.SIunits.SpecificHeatCapacity cm "Capacité thermique massique";
    parameter Modelica.SIunits.Conductivity sigma "Conductivité électrique";
  end MaterialProp;

  
  constant MaterialProp aluminium(Tf = 660.3, rho = 2.7, cm = 890, sigma = 36.9E6) "Aluminium properties";
  constant MaterialProp silver(Tf = 961.8, rho = 10.49, cm = 230, sigma = 62.1E6) "Silver properties";
  constant MaterialProp cuivre(Tf= 1085 , rho = 8.96, cm = 385, sigma = 58.7e6) " Cuivre properties";
end Materials;
