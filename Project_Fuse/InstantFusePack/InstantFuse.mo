within Project_Fuse.InstantFusePack;

model InstantFuse "Modele de fusible avec arrêt simple quand l'intensité d'entrée dépasse l'intensité nominale"
  extends base_fuse;
  Modelica.Electrical.Analog.Ideal.IdealClosingSwitch switch annotation(
    Placement(visible = true, transformation(origin = {0, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  parameter Modelica.SIunits.ElectricCurrent In = 1 "Courant Nominal";
  Boolean conducting "état de conduction";
initial equation
  conducting = true "De base, le fusible est conducteur de courant";
equation
  when i > In then
    conducting = false;
  end when "Quand le courant reçu est supérieur au courant nominal, le fusible ne conduit plus l'électricité";
  switch.control = conducting;
  connect(switch.p, p) annotation(
    Line(points = {{-10, 0}, {-100, 0}}, color = {0, 0, 255}));
  connect(switch.n, n) annotation(
    Line(points = {{10, 0}, {100, 0}}, color = {0, 0, 255}));
  annotation(
    Icon(graphics = {Rectangle(extent = {{-100, 40}, {100, -40}}), Text(origin = {1, -1}, extent = {{-101, 41}, {99, -39}}, textString = "%mat"), Text(origin = {0, 69}, lineColor = {0, 0, 255}, extent = {{-100, 29}, {100, -29}}, textString = "%name"), Line(origin = {-110, 0}, points = {{-10, 0}, {10, 0}}), Line(origin = {110, 0}, points = {{-10, 0}, {10, 0}})}));
end InstantFuse;
