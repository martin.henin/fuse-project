within Project_Fuse.InstantFusePack;

model TestInstantSimple "Modèle de test pour le fusible instantané"
  Modelica.Electrical.Analog.Basic.Ground ground annotation(
    Placement(visible = true, transformation(origin = {-86, -8}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  InstantFuse instantFuse annotation(
    Placement(visible = true, transformation(origin = {6, 20}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Sources.RampCurrent rampCurrent(I = 2)  annotation(
    Placement(visible = true, transformation(origin = {-48, 20}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(rampCurrent.n, instantFuse.p) annotation(
    Line(points = {{-38, 20}, {-4, 20}}, color = {0, 0, 255}));
  connect(instantFuse.n, ground.p) annotation(
    Line(points = {{16, 20}, {52, 20}, {52, 78}, {-86, 78}, {-86, 2}}, color = {0, 0, 255}));
  connect(rampCurrent.p, ground.p) annotation(
    Line(points = {{-58, 20}, {-86, 20}, {-86, 2}}, color = {0, 0, 255}));
annotation(
    Icon(graphics = {Ellipse(lineColor = {75, 138, 73}, fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid, extent = {{-100, -100}, {100, 100}}, endAngle = 360), Polygon(lineColor = {0, 0, 255}, fillColor = {75, 138, 73}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, points = {{-36, 60}, {64, 0}, {-36, -60}, {-36, 60}})}));

end TestInstantSimple;
