within Project_Fuse;

package InstantFusePack

  annotation(
    Icon(graphics = {Rectangle(lineColor = {200, 200, 200}, fillColor = {248, 248, 248}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-100, -100}, {100, 100}}, radius = 25), Text(extent = {{-100, 100}, {100, -100}}, textString = "I")}));
end InstantFusePack;
