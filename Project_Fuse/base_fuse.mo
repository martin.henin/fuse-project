within Project_Fuse;

model base_fuse "Classe de base pour les fusibles; contient les propriétés électroniques et matérielles du fusible"
  parameter Project_Fuse.Materials.MaterialProp mat = Project_Fuse.Materials.aluminium "Matériau du fusible";
  Modelica.SIunits.Voltage v "Voltage drop of the two pins (= p.v - n.v)";
  Modelica.SIunits.Current i "Current flowing from pin p to pin n";
  Modelica.Electrical.Analog.Interfaces.PositivePin p "Positive electrical pin" annotation (Placement(
        transformation(extent={{-110,-10},{-90,10}})));
  Modelica.Electrical.Analog.Interfaces.NegativePin n "Negative electrical pin" annotation (Placement(transformation(extent={{
  
            110,-10},{90,10}})));
equation

  v = p.v - n.v;
  i = p.i;
annotation(
    Icon(graphics = {Rectangle(extent = {{-100, 40}, {100, -40}})}));

end base_fuse;
