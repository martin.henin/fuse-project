within Project_Fuse.UniformFusePack;

model TestUniformFuse "Test the change of material of the fuse"
  extends Modelica.Icons.Example;
  //Modelica.SIunits.Time rupt_time(displayUnit "ms") "Temps de rupture";
  Real rupt_time;
  parameter Modelica.SIunits.Current I "Courant de Test";
  Project_Fuse.UniformFusePack.UniformFuse fuse(mat = Project_Fuse.Materials.aluminium)  annotation(
    Placement(visible = true, transformation(origin = {-10, 22}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Basic.Ground ground annotation(
    Placement(visible = true, transformation(origin = {-86, -8}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Sources.StepCurrent stepCurrent(I = I)  annotation(
    Placement(visible = true, transformation(origin = {-66, 22}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Logical.Switch switch1 annotation(
    Placement(visible = true, transformation(origin = {22, -42}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Continuous.Integrator integrator annotation(
    Placement(visible = true, transformation(origin = {60, -42}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Sources.Constant const0(k = 0)  annotation(
    Placement(visible = true, transformation(origin = {-56, -22}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Sources.Constant const1(k = 1)  annotation(
    Placement(visible = true, transformation(origin = {-56, -72}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  rupt_time = integrator.y;
  connect(stepCurrent.n, fuse.p) annotation(
    Line(points = {{-56, 22}, {-20, 22}}, color = {0, 0, 255}));
  connect(fuse.n, ground.p) annotation(
    Line(points = {{0, 22}, {32, 22}, {32, 50}, {-88, 50}, {-88, 2}, {-86, 2}}, color = {0, 0, 255}));
  connect(stepCurrent.p, ground.p) annotation(
    Line(points = {{-76, 22}, {-86, 22}, {-86, 2}}, color = {0, 0, 255}));
  connect(integrator.u, switch1.y) annotation(
    Line(points = {{48, -42}, {33, -42}}, color = {0, 0, 127}));
  connect(const0.y, switch1.u1) annotation(
    Line(points = {{-44, -22}, {-20, -22}, {-20, -34}, {10, -34}}, color = {0, 0, 127}));
  connect(const1.y, switch1.u3) annotation(
    Line(points = {{-44, -72}, {-20, -72}, {-20, -52}, {10, -52}, {10, -50}}, color = {0, 0, 127}));
  connect(fuse.Rupt, switch1.u2) annotation(
    Line(points = {{-10, 12}, {-12, 12}, {-12, -42}, {10, -42}}, color = {255, 0, 255}));
  annotation(
    Diagram(graphics = {Text(origin = {1, 82}, extent = {{-99, 26}, {99, -26}}, textString = "Open the Parameters dialog of the Fuse
 to change the material of the fuse 
via its mat parameter")}, coordinateSystem(initialScale = 0.1)));


end TestUniformFuse;
