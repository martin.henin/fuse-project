within Project_Fuse.UniformFusePack;

model UniformFuse "Modele d'un fusible avec température uniforme"

  extends base_fuse;
  
  Modelica.SIunits.Temperature T "Température du fusible";
  Boolean rupture "Indique si le fusible est rompu ou non";
  parameter Modelica.SIunits.Length l = 1 "Longueur du fil du fusible";
  parameter Modelica.SIunits.Length D(displayUnit = "mm") = 3e-4 "Diamètre du fil" ;
  parameter Real h = 10 "Coefficient de transfert convectif de l'air";
  final parameter Modelica.SIunits.Area S = Modelica.Constants.pi*(D/2)^2 "Section du fil";
  final parameter Modelica.SIunits.HeatCapacity Cth = mat.cm * l * S * mat.rho "Capacité thermique du fil";
  Modelica.Electrical.Analog.Basic.HeatingResistor heating_resistor(R_ref = l / (S * mat.sigma), useHeatPort = true)  annotation(
    Placement(visible = true, transformation(origin = {20, 0}, extent = {{-10, 10}, {10, -10}}, rotation = 0)));
  Modelica.Electrical.Analog.Ideal.IdealOpeningSwitch switch annotation(
    Placement(visible = true, transformation(origin = {-30, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Thermal.HeatTransfer.Components.HeatCapacitor heatCapacitor(C = Cth, T(displayUnit = "K"))  annotation(
    Placement(visible = true, transformation(origin = {-10, 40}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Thermal.HeatTransfer.Components.ThermalConductor thermalConductor(G = 3.14 * l * h * D)  annotation(
    Placement(visible = true, transformation(origin = {56, 48}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Thermal.HeatTransfer.Sources.FixedTemperature fixedTemperature annotation(
    Placement(visible = true, transformation(origin = {90, 48}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
  Modelica.Blocks.Interfaces.BooleanOutput Rupt annotation(
    Placement(visible = true, transformation(origin = {0, -108}, extent = {{-10, -10}, {10, 10}}, rotation = -90), iconTransformation(origin = {0, -44}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  Modelica.Blocks.Sources.BooleanExpression booleanExpression(y = rupture)  annotation(
    Placement(visible = true, transformation(origin = {-60, -60}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));

initial equation

  rupture=false "Initialement, le fil du fusible n'est pas altéré";
  
equation

  switch.control = rupture;
  T = heatCapacitor.T;
  when T > mat.Tf then
    rupture = true;
  end when "le fil se rompt quand sa température dépasse la température de fusion du matériau du fusible";
  connect(heating_resistor.heatPort, thermalConductor.port_a) annotation(
    Line(points = {{20, 10}, {39, 10}, {39, 48}, {46, 48}}, color = {191, 0, 0}));
  connect(switch.n, heating_resistor.p) annotation(
    Line(points = {{-20, 0}, {10, 0}}, color = {0, 0, 255}));
  connect(heating_resistor.heatPort, heatCapacitor.port) annotation(
    Line(points = {{20, 10}, {-10, 10}, {-10, 30}}, color = {191, 0, 0}));
  connect(fixedTemperature.port, thermalConductor.port_b) annotation(
    Line(points = {{80, 48}, {66, 48}}, color = {191, 0, 0}));
  connect(p, switch.p) annotation(
    Line(points = {{-100, 0}, {-40, 0}}, color = {0, 0, 255}));
  connect(heating_resistor.n, n) annotation(
    Line(points = {{30, 0}, {100, 0}}, color = {0, 0, 255}));
  connect(booleanExpression.y, Rupt) annotation(
    Line(points = {{-48, -60}, {0, -60}, {0, -108}}, color = {255, 0, 255}));
annotation(
    Icon(graphics = {Rectangle(extent = {{-100, 40}, {100, -40}}), Text(origin = {0, 70}, extent = {{-100, 30}, {100, -30}}, textString = "%name")}));

end UniformFuse;
