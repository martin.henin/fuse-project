within Project_Fuse.UniformFusePack;

model TestN
  extends Modelica.Icons.Example;
 //On créée le tableau avec les n courants d'entrée pour les n tests
  parameter Integer n = 100;
  parameter Real C_min = 1;
  parameter Real C_max = 50;
  parameter Real C[n] = linspace(C_min, C_max, n);
  // On créée le tableau dans lequel sera stocké le temps de rupture de chaque test
  Real rupt_times[n];
  //On va effectuer n fois le test TestFuse déjà défini, sur les n échelons de courant de C.
  TestUniformFuse testFuse[n](I = C)  annotation(
    Placement(visible = true, transformation(origin = {0, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
//On stock les temps de rupture de chaque test dans le tableau rupt_times
  rupt_times = testFuse.rupt_time;

annotation(
    Icon);
end TestN;
